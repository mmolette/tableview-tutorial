//
//  Player.h
//  TableViewSupinfo
//
//  Created by Mathieu Molette on 25/09/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Player : NSObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *picture;

- (id) initWithName:(NSString*)name andPicture:(NSString*)picture;

@end
