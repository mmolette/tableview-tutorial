//
//  Player.m
//  TableViewSupinfo
//
//  Created by Mathieu Molette on 25/09/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import "Player.h"

@implementation Player

- (id) initWithName:(NSString*)name andPicture:(NSString*)picture {
    
    [self setName:name];
    [self setPicture:picture];
    
    return self;
}

@end
