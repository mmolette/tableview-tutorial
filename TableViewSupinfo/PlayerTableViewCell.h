//
//  PlayerTableViewCell.h
//  TableViewSupinfo
//
//  Created by Mathieu Molette on 25/09/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *playerPicture;
@property (weak, nonatomic) IBOutlet UILabel *playerName;

@end
