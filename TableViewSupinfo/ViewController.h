//
//  ViewController.h
//  TableViewSupinfo
//
//  Created by Mathieu Molette on 24/09/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Player.h"

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) NSArray* players;

@end

