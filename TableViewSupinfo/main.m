//
//  main.m
//  TableViewSupinfo
//
//  Created by Mathieu Molette on 24/09/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
