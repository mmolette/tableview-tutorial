//
//  PlayerTableViewCell.m
//  TableViewSupinfo
//
//  Created by Mathieu Molette on 25/09/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import "PlayerTableViewCell.h"

@implementation PlayerTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
