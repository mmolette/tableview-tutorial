//
//  ViewController.m
//  TableViewSupinfo
//
//  Created by Mathieu Molette on 24/09/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import "ViewController.h"
#import "PlayerTableViewCell.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize players;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // Création des joueurs
    Player *player1 = [[Player alloc] initWithName:@"Léo Westermann" andPicture:@"westermann.png"];
    Player *player2 = [[Player alloc] initWithName:@"Nicolas Batum" andPicture:@"batum.png"];
    Player *player3 = [[Player alloc] initWithName:@"Joffrey Lauvergne" andPicture:@"lauvergne.png"];
    Player *player4 = [[Player alloc] initWithName:@"Charles Kahudi" andPicture:@"kahudi.png"];
    Player *player5 = [[Player alloc] initWithName:@"Tony Parker" andPicture:@"parker.png"];
    Player *player6 = [[Player alloc] initWithName:@"Evan Fournier" andPicture:@"fournier.png"];
    Player *player7 = [[Player alloc] initWithName:@"Florent Pietrus" andPicture:@"pietrus.png"];
    Player *player8 = [[Player alloc] initWithName:@"Nando De Colo" andPicture:@"decolo.png"];
    Player *player9 = [[Player alloc] initWithName:@"Boris Diaw" andPicture:@"diaw.png"];
    Player *player10 = [[Player alloc] initWithName:@"Mickael Gelabale" andPicture:@"gelabale"];
    Player *player11 = [[Player alloc] initWithName:@"Rudy Gobert" andPicture:@"gobert.png"];
    Player *player12 = [[Player alloc] initWithName:@"Mouhammadou Jaiteh" andPicture:@"jaiteh.png"];
    
    // Ajoute des joueurs dans un NSArray
    players = [[NSArray alloc] initWithObjects:player1, player2, player3, player4, player5, player6, player7, player8, player9, player10, player11, player12, nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Retourne le nombre d'éléments de notre liste
    return [players count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Instancie notre cellule par son identifier
    PlayerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlayerCell" forIndexPath:indexPath];
    
    // On récupère l'item que l'on va traiter
    Player *player = [players objectAtIndex:indexPath.row];
    
    // On affecte les valeurs de notre player aux éléments de notre cellule
    [cell.playerName setText:player.name];
    [cell.playerPicture setImage:[UIImage imageNamed:player.picture]];
    
    return cell;
}



@end
