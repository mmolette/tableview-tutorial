//
//  AppDelegate.h
//  TableViewSupinfo
//
//  Created by Mathieu Molette on 24/09/2015.
//  Copyright © 2015 Mathieu Molette. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

